---
title: "Homework Task 2"
author: [Severin Kaderli, Alan Kilic, Marius Schär]
date: "2018-10-01"
titlepage: true
toc-own-page: true
toc-title: "Inhaltsverzeichnis"
...

# Arithmetic Series

$$
\forall_{n} \in \mathbb{N}^{0}: \sum_{i=0}^{n}i = \frac{n(n + 1)}{2}
$$

## Initial Step

Show that $A(0)$ is correct:
$$
A(0): \sum_{i=0}^{0}0 = \frac{0 (0 + 1)}{2} = 0
$$
Show that $A(1)$ is correct:
$$
A(1): \sum_{i=0}^{1} 1 = \frac{1(1+1)}{2} = 1
$$
$A(0)$ and $A(1)$ are true.

## Induction Hypothesis

We assume that:
$$
\sum_{i=0}^{n}i = \frac{n(n+1)}{2}
$$
To prove:
$$
\sum_{i=0}^{n+1} i = \frac{(n+1) \cdot ((n+1)+1)}{2}
$$

## Inductive Step

$$
\begin{aligned}
\sum_{i=0}^{n+1} &= \sum_{i=0}^{n}i+(n+1) \\
&= \frac{n(n+1)}{2} + (n + 1) \\
&= \frac{n(n+1) + 2(n+1)}{2} \\
&= \frac{(n+1) \cdot (n+2)}{2} \\
&= \frac{(n+1)((n+1)+1)}{2}
\end{aligned}
$$

Inductive step has been verified, therefore $\displaystyle\sum_{i=0}^{n}i = \frac{n(n + 1)}{2}$ has been proven **true**.

# Geometric Series
$$
\forall_{n,x}\in\mathbb{N}^0,x \neq 0: \displaystyle\sum_{i=0}^{n} x^{i} =
\frac{x^{n + 1} - 1}{x - 1}
$$

## Initial Step
$$
\begin{aligned}
  \displaystyle\sum_{i = 0}^{0} x^{0} &= \frac{x^{0 + 1} - 1}{x - 1}\\
  x^{0} &= 1\\
  1 &= \frac{x - 1}{x - 1}
\end{aligned}
$$

## Induction Hypothesis
Assume that:
$$
\displaystyle\sum_{i = 0}^{n} x^{i} = \frac{x^{n + 1} - 1}{x - 1}
$$

To Prove:

$$
\displaystyle\sum_{i = 0}^{n + 1} x^{i} = \frac{x^{(n + 1) + 1} - 1}{x - 1}
$$

Prove that:
$$
\displaystyle\sum_{i = 0}^{n + 1} x^{i} =
\displaystyle\sum_{i = 0}^{n} x^{i} + x^{n + 1} =
\frac{x^{n + 1} - 1}{x - 1} + x^{n + 1}
$$

## Inductive Step

$$
\begin{aligned}
&\frac{x^{n + 1} - 1 + (x - 1) \cdot x^{n + 1}}{x - 1}\\
&= \frac{x^{x + 1} - 1 + x^{n + 2} - x^{n + 1}}{x - 1}\\
&= \frac{x^{n + 2} - 1}{x - 1}\\
&= \frac{x^{(n + 1) + 1} - 1}{x - 1}
\end{aligned}
$$

Inductive step has been verified, therefore $\displaystyle\sum_{i = 0}^{n} x^{i} = \frac{x^{n + 1} - 1}{x - 1}$ has been proven **true**.
