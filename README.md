# BTI7062-AlDa-Homework-2
## Task
Prove the summation laws for arithmetic and geometric series using induction.

## PDF
The ready to submit PDF file can be found [here](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-2/builds/artifacts/master/raw/TEAM-Kaderli-Kilic-Schaer.pdf?job=PDF).